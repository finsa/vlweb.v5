    <!-- Breadcrumbs -->
    <div class="container" style="margin:24px 0">
    </div>

    <div class="main-container container" id="main-container">

      <!-- Content -->
      <div class="row">
            
        <!-- post content -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box">           

            <!-- standard post -->
            <article class="entry mb-0">
              
              <div class="single-post__entry-header entry__header">
                <a href="<?=base_url().'cat/'.$detail->txt_slug?>" class="entry__meta-category entry__meta-category--label" style="background:<?=$detail->var_color?>">
                  <?=$detail->txt_posts_category?>
                </a>
                <h1 class="single-post__entry-title">
                <?=$detail->txt_posts_title?>
                </h1>

                <div class="entry__meta-holder">
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <a href="<?=base_url().'author/'.$detail->int_posts_author?>"> <?=$detail->txt_posts_author?></a>
                    </li>
                    <li class="entry__meta-date">
                    <?=idn_date($detail->dt_publish)?>
                    </li>
                  </ul>

                  <ul class="entry__meta">
                    <li class="entry__meta-views">
                      <i class="ui-eye"></i>
                      <span><?=$detail->hits[0]->int_hits_count?></span>
                    </li>
                  </ul>
                </div>
              </div> <!-- end entry header -->

              <div class="entry__img-holder">
                <img src="<?=$detail->txt_img_dir?>" alt="" class="entry__img">
                <span class="posts-img-desc"><?=$detail->txt_img_desc?></span>
              </div>
              <?=$detail->pagination?>
              <div class="entry__article-wrap">

                <!-- Share -->
                <?=$share?>

                <div class="entry__article">
                  <?=$detail->txt_posts_content?>
                  <!-- Prev / Next Post -->
                  <nav class="entry-navigation">
                    <div class="clearfix">
                      <div class="entry-navigation--right">
                          <?=$detail->next_title?>
                      </div>
                    </div>
                  </nav>
                  <div class="featured-video-posts">
                    <?=$detail->txt_posts_video ?>
                  </div>
                  <div class="fb-comments" data-href="<?=base_url().$detail->txt_posts_slug.'/'.$detail->int_posts_id?>" data-width="100%" data-numposts="5"></div>
                  <div class="ads_content">
                    <?=$this->config->item('ads_content')?>
                   </div>
                  <!-- tags -->
                  <div class="entry__tags">
                    <i class="ui-tags"></i>
                    <span class="entry__tags-label">#Tags:</span>
                    <?php foreach($detail->tag as $tag){?>
                    <a href="<?=base_url().'tag/'.$tag->txt_slug?>" rel="tag">#<?=$tag->txt_tag?></a>
                    <?php } ?>
                  </div> <!-- end tags -->

                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->
            <?=$related_posts?>
            </article> <!-- end standard post -->

          </div> <!-- end content box -->
        </div> <!-- end post content -->
