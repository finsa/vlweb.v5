<div class="entry__share">
  <div class="sticky-col">
    <div class="socials socials--rounded socials--large">
      <a class="social social-facebook" href="https://www.facebook.com/dialog/share?app_id=<?=$this->config->item('facebook_app_id')?>&display=popup&href=<?=rawurlencode($posts_url)?>" title="facebook" target="_blank" aria-label="facebook">
        <i class="ui-facebook"></i>
      </a>
      <a class="social social-twitter" href="https://twitter.com/share?text=<?=urlencode($posts_title)?>&url=<?=rawurlencode($posts_url)?>" title="twitter" target="_blank" aria-label="twitter">
        <i class="ui-twitter"></i>
      </a>
      <a class="social social-google-plus" href="https://plus.google.com/share?url=<?=rawurlencode($posts_url)?>" title="google" target="_blank" aria-label="google">
        <i class="ui-google"></i>
      </a>
      <a class="social social-whatsapp" href="whatsapp://send?text=<?=$posts_title?> | visitlumajang.com <?=urlencode($posts_url)?>" title="whatsapp" target="_blank" aria-label="whatsapp">
        <i class="ui-whatsapp"></i>
      </a>
    </div>
  </div>                  
</div> <!-- share -->
