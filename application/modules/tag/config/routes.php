<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['tag/([a-zA-Z0-9-]+)']['get']            = 'tag/index/$1';
$route['tag/([a-zA-Z0-9-]+)/([0-9]+)']['get']   = 'tag/index/$1/$2';
