<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Tag_model extends MY_Model {

    public function get_tag_posts($txt_slug = "", $start, $limit){
		$this->db->select("tp.`int_posts_id`, tp.`int_posts_author`, tp.`txt_posts_author`, tp.`int_posts_category`, tp.`txt_posts_category`,
		tp.`txt_posts_title`, tp.`txt_posts_slug`, tp.`dt_publish`, tpi.`txt_dir`, mt.*")
					->from($this->t_posts.' tp')
					->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
					->join($this->t_posts_tag.' tpt', 'tp.`int_posts_id` = tpt.`int_posts_id`', 'LEFT')
					->join($this->m_tag.' mt', 'tpt.`int_tag_id` = mt.`int_tag_id`', 'LEFT')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where('mt.`txt_slug`', $txt_slug)
					->group_by("tp.`int_posts_id`");
		$order = 'dt_publish ';
		$sort = 'DESC';		
		if($limit > 0){
			$this->db->limit($limit, $start);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}

    public function count_tag_posts($txt_slug = ""){
		$this->db->select('tp.`int_posts_id`')
					->from($this->t_posts.' tp')
					->join($this->t_posts_tag.' tpt', 'tp.`int_posts_id` = tpt.`int_posts_id`', 'LEFT')
					->join($this->m_tag.' mt', 'tpt.`int_tag_id` = mt.`int_tag_id`', 'LEFT')
					->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
					->where('mt.`txt_slug`', $txt_slug);

		return $this->db->count_all_results();
	}
}