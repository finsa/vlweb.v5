    <!-- Breadcrumbs -->
    <div class="container" style="margin:24px 0">
    </div>
    

    <div class="main-container container" id="main-container">         

      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content mb-72">
          <h1 class="page-title" style="background:<?=$category_posts[0]->var_color?>"><?=$category_posts[0]->txt_category?></h1>

          <div class="row card-row">
          <?php foreach ($category_posts as $posts){ ?>
            <div class="col-md-6">
              <article class="entry card">
                <div class="entry__img-holder card__img-holder">
                  <a href="<?=base_url().$posts->txt_posts_slug.'/'.$posts->int_posts_id?>">
                    <div class="thumb-container thumb-56">
                      <img data-src="<?=cdn_url().$posts->txt_dir?>" src="<?php echo base_url() ?>v5/img/empty.png" class="entry__img lazyload" alt="" />
                    </div>
                  </a>
                  <a href="<?=base_url().'cat/'.$posts->txt_slug?>" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner" style="background:<?=$posts->var_color?>">
                    <?=$posts->txt_posts_category?>
                  </a>
                </div>

                <div class="entry__body card__body">
                  <div class="entry__header">
                    
                    <h2 class="entry__title">
                      <a href="<?=base_url().$posts->txt_posts_slug.'/'.$posts->int_posts_id?>">
                      <?=$posts->txt_posts_title?>
                      </a>
                    </h2>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <a href="<?=base_url().'author/'.$posts->int_posts_author?>">
                          <?=$posts->txt_posts_author?>
                        </a>
                      </li>
                      <li class="entry__meta-date">
                        <?=idn_date($posts->dt_publish)?>
                      </li>
                    </ul>
                  </div>
                </div>
              </article>
            </div>
          <?php } ?>
          </div>

          <?=$cat_pagination?>
        </div> <!-- end posts -->
