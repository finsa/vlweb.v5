<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Homepage_model extends MY_Model {

    public function get_posts_list($type = "", $category = "", $start, $limit){
		$this->db->select("tp.`int_posts_id`, tp.`int_posts_author`, tp.`txt_posts_author`, tp.`int_posts_category`, tp.`txt_posts_category`,
		tp.`txt_posts_title`, tp.`txt_posts_slug`, tp.`dt_publish`, tpi.`txt_dir`, mc.`txt_slug`, mc.`var_color`")
					->from($this->t_posts.' tp')
					->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
					->join($this->m_category.' mc', 'tp.`int_posts_category` = mc.`int_category_id`', 'LEFT')
					->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
					->group_by("tp.`int_posts_id`");

		if($type != ""){ // filter
			$this->db->where('tp.`int_posts_type` IN ('.$type.')');
		}

		if($category != ""){ // filter
			$this->db->where('tp.`int_posts_category` IN ('.$category.')');
		}

		$order = 'dt_publish ';
		$sort = 'DESC';		
		if($limit > 0){
			$this->db->limit($limit, $start);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}

    public function count_posts_list($type = "", $category = ""){
		$this->db->select('tp.`int_posts_id`')
					->from($this->t_posts.' tp')
					->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'');

		if($type != ""){ // filter
			$this->db->where('tp.`int_posts_type` IN ('.$type.')');
		}

		if($category != ""){ // filter
			$this->db->where('tp.`int_posts_category` IN ('.$category.')');
		}

		return $this->db->count_all_results();
	}
}