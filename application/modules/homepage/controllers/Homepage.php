<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller {
    var $limit = "10";

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		//$this->authCheck();
		
		$this->load->model('sidebar_model', 'sidebar');
		$this->load->model('homepage_model', 'model');
    }
	
	public function index($page = 1){
		$start = $this->limit * ($page - 1);
		
        //$this->model->get_posts($type, $category, $start, $limit);
        $data['latest_event'] = $this->model->get_posts_list("", "3", 0, 5);
        $data['headline_posts'] = $this->model->get_posts_list("", "1,2", 0, 6);
        $data['latest_posts'] = $this->model->get_posts_list("", "", $start, $this->limit);
        $data['advetorial_posts'] = $this->model->get_posts_list("4", "", 0, 4);
        $data['latest_humaniora'] = $this->model->get_posts_list("", "4", 0, 5);
        $data['latest_kuliner'] = $this->model->get_posts_list("", "2", 0, 5);
        $data['latest_belanja'] = $this->model->get_posts_list("", "5", 0, 5);
        $data['latest_event'] = $this->model->get_posts_list("", "3", 0, 5);
        $data['latest_media_partner'] = $this->model->get_posts_list("", "8", 0, 5);
        $data['popular'] = $this->sidebar->get_popular();
        //print_r($data_pelaporan);
        $count_posts = $this->model->count_posts_list();
		$data["pagination"] = $this->_build_pagination($page,$count_posts,$this->limit,base_url()."index/");

		$this->render_view('index', $data, true, 'homepage');
	}

	public function index_list($page = 1){
		$start = $this->limit * ($page - 1);

        $this->page->title = 'Index';
		$this->page->meta_description = 'Portal Informasi Pariwisata Lumajang';
		$this->page->meta_keywords = 'Pariwisata, Lumajang, Semeru, Pantai, Pasir, Gunung';
		$this->page->meta_url = base_url();
		$this->page->meta_image = cdn_url().'v5/img/vl.png';
		
        //$this->model->get_posts($type, $category, $start, $limit);
        $data['latest_posts'] = $this->model->get_posts_list("", "", $start, $this->limit);

        //print_r($data_pelaporan);
        $count_posts = $this->model->count_posts_list();
		$data["pagination"] = $this->_build_pagination($page,$count_posts,$this->limit,base_url()."index/");

		$this->render_view('index_list', $data, true, 'template');
	}
}
