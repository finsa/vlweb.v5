      </div> <!-- end content -->

      <!-- Ad Banner 728 -->
      <div class="text-center pb-48">
        <?=$this->config->item('ads_homepage')?>
      </div>
      <!-- Advetorial posts -->
      <section class="section mb-0">
        <div class="title-wrap title-wrap--line title-wrap--pr">
          <h3 class="section-title">Advetorial</h3>
        </div>

        <!-- Slider -->
        <div id="owl-posts" class="owl-carousel owl-theme owl-carousel--arrows-outside">
		      <?php foreach ($advetorial_posts as $ads){?>
          <article class="entry thumb thumb--size-1">
            <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$ads->txt_dir?>');">
              <div class="bottom-gradient"></div>
              <div class="thumb-text-holder">   
                <h2 class="thumb-entry-title">
                  <a href="<?=base_url().$ads->txt_posts_slug.'/'.$ads->int_posts_id?>">
                    <?=$ads->txt_posts_title?>
                  </a>
                </h2>
              </div>
              <a href="<?=base_url().$ads->txt_posts_slug.'/'.$ads->int_posts_id?>" class="thumb-url"></a>
            </div>
          </article>
		      <?php } ?>
        </div> <!-- end slider -->

      </section> <!-- end Advetorial posts -->
      <!-- Posts from categories -->
      <section class="section mb-0">
        <div class="row">

          <!-- Kuliner -->
          <div class="col-md-6">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Kuliner</h3>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-2">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$latest_kuliner[0]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--1">   
                      <h2 class="thumb-entry-title">
                      <a href="<?=base_url().$latest_kuliner[0]->txt_posts_slug.'/'.$latest_kuliner[0]->int_posts_id?>">
                        <?=$latest_kuliner[0]->txt_posts_title?>
                      </a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <a href="<?=base_url().'author/'.$latest_kuliner[0]->int_posts_author?>">
                            <?=$latest_kuliner[0]->txt_posts_author?>
                          </a>
                        </li>
                        <li class="entry__meta-date">
						            	<?=idn_date($latest_kuliner[0]->dt_publish)?>
                        </li>
                      </ul>
                    </div>
                    <a href="<?=base_url().$latest_kuliner[0]->txt_posts_slug.'/'.$latest_kuliner[0]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
              <div class="col-lg-6">
                <ul class="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
				          <?php for ($k = 1; $k <= 4; $k++) { ?>
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry">
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="<?=base_url().$latest_kuliner[$k]->txt_posts_slug.'/'.$latest_kuliner[$k]->int_posts_id?>">
                            <?=$latest_kuliner[$k]->txt_posts_title?>
                          </a>
                        </h3>
                      </div>                  
                    </article>
                  </li>
				          <?php } ?>
                </ul>
              </div>
            </div>            
          </div> <!-- end  -->

          <!-- Belanja -->
          <div class="col-md-6">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Belanja</h3>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-2">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$latest_belanja[0]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--1">   
                      <h2 class="thumb-entry-title">
                        <a href="<?=base_url().$latest_belanja[0]->txt_posts_slug.'/'.$latest_belanja[0]->int_posts_id?>">
                          <?=$latest_belanja[0]->txt_posts_title?>
                        </a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <a href="<?=base_url().'author/'.$latest_belanja[0]->int_posts_author?>">
                            <?=$latest_belanja[0]->txt_posts_author?>
                          </a>
                        </li>
                        <li class="entry__meta-date">
							            <?=idn_date($latest_belanja[0]->dt_publish)?>
                        </li>
                      </ul>
                    </div>
                    <a href="<?=base_url().$latest_belanja[0]->txt_posts_slug.'/'.$latest_belanja[0]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
              <div class="col-lg-6">
                <ul class="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
    				      <?php for ($k = 1; $k <= 4; $k++) { ?>
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry">
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="<?=base_url().$latest_belanja[$k]->txt_posts_slug.'/'.$latest_belanja[$k]->int_posts_id?>">
                            <?=$latest_belanja[$k]->txt_posts_title?>
                          </a>
                        </h3>
                      </div>                  
                    </article>
                  </li>
				          <?php } ?>
                </ul>
              </div>
            </div>            
          </div> <!-- end  -->

          <!-- Humaniora -->
          <div class="col-md-6">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Humaniora</h3>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-2">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$latest_humaniora[0]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--1">   
                      <h2 class="thumb-entry-title">
                        <a href="<?=base_url().$latest_humaniora[0]->txt_posts_slug.'/'.$latest_humaniora[0]->int_posts_id?>">
                          <?=$latest_humaniora[0]->txt_posts_title?>
                        </a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <a href="<?=base_url().'author/'.$latest_humaniora[0]->int_posts_author?>">
                            <?=$latest_humaniora[0]->txt_posts_author?>
                          </a>
                        </li>
                        <li class="entry__meta-date">
							            <?=idn_date($latest_humaniora[0]->dt_publish)?>
                        </li>
                      </ul>
                    </div>
                    <a href="<?=base_url().$latest_humaniora[0]->txt_posts_slug.'/'.$latest_humaniora[0]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
              <div class="col-lg-6">
                <ul class="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
				          <?php for ($k = 1; $k <= 4; $k++) { ?>
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry">
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="<?=base_url().$latest_humaniora[$k]->txt_posts_slug.'/'.$latest_humaniora[$k]->int_posts_id?>">
                            <?=$latest_humaniora[$k]->txt_posts_title?>
                          </a>
                        </h3>
                      </div>                  
                    </article>
                  </li>
				          <?php } ?>
                </ul>
              </div>
            </div>            
          </div> <!-- end  -->

          <!-- Event -->
          <div class="col-md-6">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Event</h3>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-2">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$latest_event[0]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--1">   
                      <h2 class="thumb-entry-title">
                        <a href="<?=base_url().$latest_event[0]->txt_posts_slug.'/'.$latest_event[0]->int_posts_id?>">
                          <?=$latest_event[0]->txt_posts_title?>
                        </a>
                      </h2>
                      <ul class="entry__meta">
                        <li class="entry__meta-author">
                          <a href="<?=base_url().'author/'.$latest_event[0]->int_posts_author?>">
                            <?=$latest_event[0]->txt_posts_author?>
                          </a>
                        </li>
                        <li class="entry__meta-date">
							            <?=idn_date($latest_event[0]->dt_publish)?>
                        </li>
                      </ul>
                    </div>
                    <a href="<?=base_url().$latest_event[0]->txt_posts_slug.'/'.$latest_event[0]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
              <div class="col-lg-6">
                <ul class="post-list-small post-list-small--dividers post-list-small--arrows mb-24">
				          <?php for ($k = 1; $k <= 4; $k++) { ?>
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry">
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="<?=base_url().$latest_event[$k]->txt_posts_slug.'/'.$latest_event[$k]->int_posts_id?>">
                            <?=$latest_event[$k]->txt_posts_title?>
                          </a>
                        </h3>
                      </div>                  
                    </article>
                  </li>
				          <?php } ?>
                </ul>
              </div>
            </div>            
          </div> <!-- end  -->

        </div>                
      </section> <!-- end posts from categories -->
     <?=$this->config->item('ads_homepage')?>
    </div> <!-- end main container -->
