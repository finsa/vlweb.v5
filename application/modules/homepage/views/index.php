  <!-- Trending Now -->
  <div class="container">
      <div class="trending-now">
        <span class="trending-now__label">
          <i class="ui-flash"></i>
          <span class="trending-now__text d-lg-inline-block d-none">Event</span>
        </span>
        <div class="newsticker">
          <ul class="newsticker__list">
			<?php foreach ($latest_event as $event) {?>
            <li class="newsticker__item">
				<a href="<?=base_url().$event->txt_posts_slug.'/'.$event->int_posts_id?>" class="newsticker__item-url">
				<?=$event->txt_posts_title?>
				</a>
			</li>
			<?php } ?>
          </ul>
        </div>
        <div class="newsticker-buttons">
          <button class="newsticker-button newsticker-button--prev" id="newsticker-button--prev" aria-label="next article"><i class="ui-arrow-left"></i></button>
          <button class="newsticker-button newsticker-button--next" id="newsticker-button--next" aria-label="previous article"><i class="ui-arrow-right"></i></button>
        </div>
      </div>
    </div>
    <!-- Featured Posts Grid -->      
    <section class="featured-posts-grid featured-posts-grid--1">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <article class="entry thumb thumb--size-4">
              <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$headline_posts[0]->txt_dir?>');">
                <div class="bottom-gradient"></div>
                <div class="thumb-text-holder thumb-text-holder--3">
                  <a href="<?=base_url().'cat/'.$headline_posts[0]->txt_slug?>"  class="entry__meta-category entry__meta-category--label" style="background:<?=$headline_posts[0]->var_color?>">
					  <?=$headline_posts[0]->txt_posts_category?>
				  </a>
                  <ul class="entry__meta">
                    <li class="entry__meta-date">
					<?=idn_date($headline_posts[0]->dt_publish)?>
                    </li>
                  </ul>
                  <h2 class="thumb-entry-title">
					<a href="<?=base_url().$headline_posts[0]->txt_posts_slug.'/'.$headline_posts[0]->int_posts_id?>">
						<?=$headline_posts[0]->txt_posts_title?>
					</a>
                  </h2>
                </div>
                <a href="<?=base_url().$headline_posts[0]->txt_posts_slug.'/'.$headline_posts[0]->int_posts_id?>" class="thumb-url"></a>
              </div>
            </article>

            <div class="row">
			<?php for ($x = 2; $x <= 3; $x++) { ?>
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-1">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$headline_posts[$x]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--4">
                      <h2 class="thumb-entry-title">
                        <a href="<?=base_url().$headline_posts[$x]->txt_posts_slug.'/'.$headline_posts[$x]->int_posts_id?>"><?=$headline_posts[$x]->txt_posts_title?></a>
                      </h2>
                    </div>
                    <a href="<?=base_url().$headline_posts[$x]->txt_posts_slug.'/'.$headline_posts[$x]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
			<?php } ?>
            </div> <!-- end row -->

          </div> <!-- end col -->
          <div class="col-lg-6">
            <article class="entry thumb thumb--size-4">
              <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$headline_posts[1]->txt_dir?>');">
                <div class="bottom-gradient"></div>
                <div class="thumb-text-holder thumb-text-holder--3">
                  <a href="<?=base_url().'cat/'.$headline_posts[1]->txt_slug?>" class="entry__meta-category entry__meta-category--label" style="background:<?=$headline_posts[1]->var_color?>">
                    <?=$headline_posts[1]->txt_posts_category?>
                  </a>
                  <ul class="entry__meta">
                    <li class="entry__meta-date">
					          <?=idn_date($headline_posts[1]->dt_publish)?>
                    </li>
                  </ul>
                  <h2 class="thumb-entry-title">
                  <a href="<?=base_url().$headline_posts[1]->txt_posts_slug.'/'.$headline_posts[1]->int_posts_id?>">
                    <?=$headline_posts[1]->txt_posts_title?>
                  </a>
                  </h2>
                </div>
                <a href="<?=base_url().$headline_posts[1]->txt_posts_slug.'/'.$headline_posts[1]->int_posts_id?>" class="thumb-url"></a>
              </div>
            </article>

            <div class="row">
			      <?php for ($x = 4; $x <= 5; $x++) { ?>
              <div class="col-lg-6">
                <article class="entry thumb thumb--size-1">
                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=cdn_url().$headline_posts[$x]->txt_dir?>');">
                    <div class="bottom-gradient"></div>
                    <div class="thumb-text-holder thumb-text-holder--4">
                      <h2 class="thumb-entry-title">
                        <a href="<?=base_url().$headline_posts[$x]->txt_posts_slug.'/'.$headline_posts[$x]->int_posts_id?>"><?=$headline_posts[$x]->txt_posts_title?></a>
                      </h2>
                    </div>
                    <a href="<?=base_url().$headline_posts[$x]->txt_posts_slug.'/'.$headline_posts[$x]->int_posts_id?>" class="thumb-url"></a>
                  </div>
                </article>
              </div>
			        <?php } ?>
            </div> <!-- end row -->

          </div> <!-- end col -->

        </div>
      </div>
    </section> <!-- end featured posts grid -->

    <div class="main-container container pt-24" id="main-container">         
      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content">
          
          <!-- Latest News -->
          <section class="section tab-post mb-16">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Latest Posts</h3>
            </div>

            <!-- tab content -->
            <div class="tabs__content tabs__content-trigger tab-post__tabs-content">
              
              <div class="tabs__content-pane tabs__content-pane--active" id="tab-all">
                <div class="row card-row">
				        <?php foreach ($latest_posts as $lp){?>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="<?=base_url().$lp->txt_posts_slug.'/'.$lp->int_posts_id?>">
                          <div class="thumb-container thumb-56">
                            <img data-src="<?=cdn_url().$lp->txt_dir?>" src="<?php echo base_url() ?>v5/img/empty.png" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="<?=base_url().'cat/'.$lp->txt_slug?>" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner" style="background:<?=$lp->var_color?>">
                          <?=$lp->txt_posts_category?>
                        </a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="<?=base_url().$lp->txt_posts_slug.'/'.$lp->int_posts_id?>">
                            <?=$lp->txt_posts_title?>
                            </a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-date">
								              <?=idn_date($lp->dt_publish)?>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </article>
                  </div>
				            <?php } ?>
                </div>
              </div> <!-- end pane 1 -->
            </div> <!-- end tab content -->
            <!-- Pagination -->
            <?=$pagination?>
              
          </section> <!-- end latest news -->

        </div> <!-- end posts -->