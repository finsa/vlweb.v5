        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Popular Posts</h4>
            <ul class="post-list-small">
			      <?php foreach($popular as $pop){ ?>
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="<?=base_url().$pop->txt_posts_slug.'/'.$pop->int_posts_id?>">
                        <img data-src="<?=cdn_url().$pop->txt_dir?>" src="<?php echo base_url() ?>v5/img/empty.png" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="<?=base_url().$pop->txt_posts_slug.'/'.$pop->int_posts_id?>">
                        <?=$pop->txt_posts_title?>
                      </a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-date">
					              <?=idn_date($pop->dt_publish)?>
                      </li>
                    </ul>
                  </div>                  
                </article>
              </li>
			      <?php } ?>
            </ul>           
          </aside> <!-- end widget popular posts -->

          <!-- Widget Socials --
          <aside class="widget widget-socials">
            <h4 class="widget-title">Social Media</h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="https://www.facebook.com/visitlumajang/" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a>
                  <a class="social social-instagram" href="https://www.instagram.com/visitlumajang/" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a>
                  <a class="social social-twitter" href="https://www.twitter.com/visitlumajang/" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a>
                  <a class="social social-youtube" href="https://www.youtube.com/channel/UCohhENiJN0QiorbISKXoEGw" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                  </a>
                </div>                
              </div>            
            </div>
          </aside> <!-- end widget socials -->

          <!-- Widget Media Partner -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Media Partner</h4>
            <ul class="post-list-small">
			      <?php foreach($latest_media_partner as $lmp){ ?>
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="<?=base_url().$lmp->txt_posts_slug.'/'.$lmp->int_posts_id?>">
                        <img data-src="<?=cdn_url().$lmp->txt_dir?>" src="<?php echo base_url() ?>v5/img/empty.png" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="<?=base_url().$lmp->txt_posts_slug.'/'.$lmp->int_posts_id?>">
                        <?=$lmp->txt_posts_title?>
                      </a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-date">
					              <?=idn_date($lmp->dt_publish)?>
                      </li>
                    </ul>
                  </div>                  
                </article>
              </li>
			        <?php } ?>
            </ul>           
          </aside> <!-- end  -->
        </aside> <!-- end sidebar -->