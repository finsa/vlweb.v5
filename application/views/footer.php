    <!-- Footer -->
    <footer class="footer footer--dark">
      <div class="container">
        <div class="footer__widgets">
          <div class="row">

            <div class="col-lg-6 col-md-6">
              <aside class="widget widget-logo">
                <p class="copyright">
                  ©2020 visitlumajang.com by Pelangi Teknomedia Nusantara</a>
                </p>
              </aside>
            </div>

          </div>
        </div>    
      </div> <!-- end container -->
    </footer> <!-- end footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>
