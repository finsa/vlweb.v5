<div class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?php echo $data->header?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
            <div class="alert alert-danger">
                  <h5><i class="icon fas fa-ban"></i> <?php echo $data->title?></h5>
                  <?php echo $data->message?>
            </div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-warning">Batal</button>
		</div>
	</div>
</div>