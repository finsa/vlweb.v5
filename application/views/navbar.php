  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->    
  <header class="sidenav" id="sidenav">

    <!-- close -->
    <div class="sidenav__close">
      <button class="sidenav__close-button" id="sidenav__close-button" aria-label="close sidenav">
        <i class="ui-close sidenav__close-icon"></i>
      </button>
    </div>
    
    <!-- Nav -->
    <nav class="sidenav__menu-container">
      <ul class="sidenav__menu" role="menubar">
        <li><a class="sidenav__menu-url" href="<?=base_url()?>">Home</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/wisata">Wisata</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/kuliner">Kuliner</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/belanja">Belanja</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/humaniora">Humaniora</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/event">Event</a></li>
        <li><a class="sidenav__menu-url" href="<?=base_url()?>cat/akomodasi">Akomodasi</a></li>
      </ul>
    </nav>

    <div class="socials sidenav__socials"> 
      <a class="social social-facebook" href="https://www.facebook.com/visitlumajang/" target="_blank" aria-label="facebook">
        <i class="ui-facebook"></i>
      </a>
      <a class="social social-instagram" href="https://www.instagram.com/visitlumajang/" target="_blank" aria-label="instagram">
        <i class="ui-instagram"></i>
      </a>
      <a class="social social-twitter" href="https://www.twitter.com/visitlumajang/" target="_blank" aria-label="twitter">
        <i class="ui-twitter"></i>
      </a>
      <a class="social social-youtube" href="https://www.youtube.com/channel/UCohhENiJN0QiorbISKXoEGw" target="_blank" aria-label="youtube">
        <i class="ui-youtube"></i>
      </a>
    </div>
  </header> <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Navigation -->
    <header class="nav">
      <div class="nav__holder nav--sticky">
        <div class="container relative">
          <div class="flex-parent">

            <!-- Side Menu Button -->
            <button class="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open side menu">
              <span class="nav-icon-toggle__box">
                <span class="nav-icon-toggle__inner"></span>
              </span>
            </button> 

            <!-- Logo -->
            <a href="<?=base_url()?>" class="logo">
              <img class="logo__img" src="<?php echo base_url() ?>v5/img/vl.png" srcset="<?php echo base_url() ?>v5/img/vl.png" alt="logo">
            </a>

            <!-- Nav-wrap -->
            <nav class="flex-child nav__wrap d-none d-lg-block">              
              <ul class="nav__menu">
                <li class="">
                  <a href="<?=base_url()?>">Home</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/wisata">Wisata</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/kuliner">Kuliner</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/belanja">Belanja</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/humaniora">Humaniora</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/event">Event</a>
                </li>
                <li class="">
                  <a href="<?=base_url()?>cat/akomodasi">Akomodasi</a>
                </li>
                <li class="nav__dropdown">
                  <a href="#">Visitlumajang</a>
                  <ul class="nav__dropdown-menu">
                    <li><a href="<?=base_url()?>about-us">About Us</a></li>
                    <li><a href="<?=base_url()?>advertisement">Advertisement</a></li>
                    <li><a href="<?=base_url()?>terms-of-service">Term of Service</a></li>
                    <li><a href="<?=base_url()?>privacy-policy">Privacy Policy</a></li>
                    <li><a href="<?=base_url()?>copyright">Copyright</a></li>
                    <li><a href="<?=base_url()?>disclaimer">Disclaimer</a></li>
                    <li><a href="<?=base_url()?>contact">Contact</a></li>
                  </ul>
                </li>                


              </ul> <!-- end menu -->
            </nav> <!-- end nav-wrap -->

            <!-- Nav Right -->
            <div class="nav__right">

              <!-- Search -->
              <div class="nav__right-item nav__search">
                <a href="#" class="nav__search-trigger" id="nav__search-trigger">
                  <i class="ui-search nav__search-trigger-icon"></i>
                </a>
                <div class="nav__search-box" id="nav__search-box">
                <script async src="https://cse.google.com/cse.js?cx=012284878125875172452:u6e-oykq6ze"></script>
<div class="gcse-search"></div>
                </div>                
              </div>             

            </div> <!-- end nav right -->            
        
          </div> <!-- end flex-parent -->
        </div> <!-- end container -->

      </div>
    </header> <!-- end navigation -->
