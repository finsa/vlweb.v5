<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $page->title ?> | VisitLumajang</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $page->meta_description ?>" />
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>" />
    <meta name="author" content="<?php echo $this->config->item('page_title')?> | by. Pelangi Teknomedia Nusantara" />
    <meta property="og:title" content="<?php echo $page->title ?>"/>
    <meta property="og:description" content="<?php echo $page->meta_description ?>" />
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo $page->meta_url ?>"/>
    <meta property="og:image" content="<?php echo $page->meta_image?>"/>
    <meta property="og:site_name" content="<?php echo $this->config->item('page_title')?> | by. Pelangi Teknomedia Nusantara" />
    <meta property="fb:app_id" content="<?php echo $this->config->item('facebook_app_id')?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>

  <!-- Css -->
  <link rel="stylesheet" href="<?php echo base_url() ?>v5/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>v5/css/font-icons.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>v5/css/style.css" />

  <!-- Favicons -->
  <link rel="shortcut icon" href="<?php echo base_url() ?>v5/img/icon.png">

  <script data-ad-client="ca-pub-4107184004444393" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

  <!-- Lazyload (must be placed in head in order to work) -->
  <script src="<?php echo base_url() ?>v5/js/lazysizes.min.js"></script>

</head>

<body class="bg-light style-default style-rounded">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>

  <?=$navbar?>

  <?=$content?>
  <?=$sidebar?>
  </div></div>
  <?=$footer?>

  </main> <!-- end main-wrapper -->

  
  <!-- jQuery Scripts -->
  <script src="<?php echo base_url() ?>v5/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/easing.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/owl-carousel.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/flickity.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/twitterFetcher_min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/jquery.sticky-kit.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/jquery.newsTicker.min.js"></script>  
  <script src="<?php echo base_url() ?>v5/js/modernizr.min.js"></script>
  <script src="<?php echo base_url() ?>v5/js/scripts.js"></script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1295393790551087',
          xfbml      : true,
          version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
      };
    
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-65838559-6', 'auto');
      ga('send', 'pageview');
    </script>
</body>
</html>