<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Sidebar_model extends MY_Model {

	public function get_popular(){
		$date = date_create(date('Y-m-d'));
		date_sub($date,date_interval_create_from_date_string("30 days"));
		$dt_start = date_format($date,"Y-m-d H:i:s");

		$this->db->select("th.`int_hits_count`, tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`, tp.`dt_publish`, tpi.`txt_dir`")
					->from($this->t_posts.' tp')
					->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
					->join($this->t_hits.' th', 'tp.`int_posts_id` = th.`int_posts_id`', 'LEFT')
					->where("tp.dt_publish BETWEEN '".$dt_start."' AND '".date("Y-m-d H:i:s")."'");

		$order = 'int_hits_count ';
		$sort = 'DESC';
		$this->db->limit(5);
        return $this->db->order_by($order, $sort)->get()->result();
    }

    public function get_media_partner(){
        $this->db->select("tp.`int_posts_id`, tp.`txt_posts_title`, tp.`txt_posts_slug`, tp.`dt_publish`, tpi.`txt_dir`")
                    ->from($this->t_posts.' tp')
                    ->join($this->t_posts_img.' tpi', 'tp.`int_posts_id` = tpi.`int_posts_id`', 'LEFT')
                    ->where('tp.int_status = 2 AND tp.dt_publish < \''.date('Y-m-d H:i:s').'\'')
                    ->where('tp.`int_posts_category`', 8)
                    ->limit(5)
                    ->order_by('tp.`dt_publish`', 'DESC')
                    ->group_by("tp.`int_posts_id`");

        return $this->db->get()->result();
    }
}